resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

# Creating subnets looping over count
resource "aws_subnet" "subnet_count" {
  count      = 2
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.${count.index + 1}.0/24"
}

# Creating subnets looping using for_each against an object
resource "aws_subnet" "subnet_for_each" {
  for_each = {
    subnet_3_cidr = "10.0.3.0/24"
    subnet_4_cidr = "10.0.4.0/24"
  }
  vpc_id     = aws_vpc.main.id
  cidr_block = each.value
}

# Creating security groups looping over another resource
resource "aws_security_group" "sg_for_subnet" {
  for_each = aws_subnet.subnet_for_each
  vpc_id   = aws_vpc.main.id
}


