# Simple list construct, can iterate over with for_each and count
variable "subnets_list" {
  type = list(string)
  default = [
    "10.0.3.0/24",
    "10.0.4.0/24"
  ]
}

# Map construct, loose and unstructured data type - collection of values where each is identified by a string label. Can iterate over with for_each, not count
variable "subnets_map" {
  type = map(any)
  default = {
    subnet_ip_1 = "10.0.3.0/24",
    subnet_ip_2 = "10.0.4.0/24"
  }
}

# List of maps
variable "subnets_list_map" {
  type = list(map(any))
  default = [
    { subnet_ip_1 = "10.0.3.0/24" },
    { subnet_ip_2 = "10.0.4.0/24" }
  ]
}

# Object construct, structural data type, required to be in the form { KEY = TYPE }
variable "subnet_object" {
  type = object({ subnet_ip = string, subnet_name = string, create = bool })
  default = {
    subnet_ip = "10.0.3.0/24", subnet_name = "example", create = false
  }
}

# Object constructs in a list, structural data type, required to be in the form { KEY = TYPE }
variable "subnets_object_list" {
  type = list(object({ subnet_ip = string, name = string, create = bool }))
  default = [
    { subnet_ip = "10.0.3.0/24", name = "example", create = false },
    { subnet_ip = "10.0.4.0/24", name = "example2", create = true }
  ]
}